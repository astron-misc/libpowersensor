#include <dirent.h>
#include <string.h>

#include <iostream>
#include <cstdio>
#include <string>
#include <sstream>
#include <vector>

#include "rocm_smi/rocm_smi.h"

#include "ROCMPowerSensor.h"

namespace powersensor {
namespace rocm {

class ROCMPowerSensor_ : public ROCMPowerSensor {
    public:
        ROCMPowerSensor_(const unsigned device_number);
        ~ROCMPowerSensor_();

    private:
        class ROCMState{
            public:
                operator State();
                double timeAtRead;
                double instantaneousPower   = 0;
                double consumedEnergyDevice = 0;
        };

    virtual State measure();

    virtual const char* getDumpFileName() {
        return "/tmp/rocmpowersensor.out";
    }

    virtual int getDumpInterval() {
        return 10; // milliseconds
    }

    unsigned int _device_number;

    ROCMState previousState;
    ROCMState read_rocm();
};

ROCMPowerSensor_::ROCMState::operator State()
{
    State state;
    state.timeAtRead = timeAtRead;
    state.joulesAtRead = consumedEnergyDevice;
    return state;
}

ROCMPowerSensor* ROCMPowerSensor::create(
    int device_number)
{
    rsmi_status_t ret;
    ret = rsmi_init(0);
    if(ret == RSMI_STATUS_PERMISSION || ret != RSMI_STATUS_SUCCESS)
    {
        std::cout << "ROCM-SMI initialization failed" << std::endl;
        exit(EXIT_FAILURE);
    }
    return new ROCMPowerSensor_(device_number);
}

ROCMPowerSensor_::ROCMPowerSensor_(const unsigned device_number)
{
    _device_number = device_number;

    State startState = read_rocm();
}

float get_power(unsigned device_number)
{
    rsmi_status_t ret;
    uint64_t val_ui64;
    uint32_t i=0;
    ret = rsmi_dev_power_ave_get(device_number, 0, &val_ui64);

    if(ret == RSMI_STATUS_PERMISSION || ret != RSMI_STATUS_SUCCESS)
    {
        std::cout << "ROCM-SMI read failed" << std::endl;
        exit(EXIT_FAILURE);
    }
  
    return static_cast<float>(val_ui64)*1e-6;
}

ROCMPowerSensor_::ROCMState ROCMPowerSensor_::read_rocm() {
    ROCMState state;
    state.timeAtRead = get_wtime();
    state.instantaneousPower = get_power(_device_number);
    state.consumedEnergyDevice = previousState.consumedEnergyDevice;
    float averagePower = (state.instantaneousPower + previousState.instantaneousPower) / 2;
    float timeElapsed = (state.timeAtRead - previousState.timeAtRead);
    state.consumedEnergyDevice += averagePower * timeElapsed;
    previousState = state;
    return state;
}

ROCMPowerSensor_::~ROCMPowerSensor_() {
    stopDumpThread();
    rsmi_shut_down();
}

State ROCMPowerSensor_::measure() {
    return read_rocm();
}

} // end namespace rocm
} // end namespace powersensor
