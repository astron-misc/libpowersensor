project(dummy)

add_library(
    powersensor-dummy
    OBJECT
    DummyPowerSensor.cpp
)

install(
    FILES
    DummyPowerSensor.h
    DESTINATION
    include/powersensor
)

target_link_libraries(
    powersensor
    PUBLIC
    powersensor-dummy
)