import sys
import time

import pypowersensor as ps

def powersensor(platform, device_id = 0):
    def decorator(func):
        def wrapper(*args, **kwargs):
            if platform == "amdgpu":
                powersensor = ps.AMDGPUPowerSensor.create(device_id)
            elif platform == "arduino":
                try:
                    powersensor = ps.ArduinoPowerSensor.create(device_id)
                except AttributeError:
                    print("ArduinoPowerSensor not installed")
                    sys.exit(1)
            elif platform == "dummy":
                powersensor = ps.DummyPowerSensor.create()
            elif platform == "jetson":
                powersensor = ps.JetsonPowerSensor.create()
            elif platform == "likwid":
                try:
                    powersensor = ps.LikwidPowerSensor.create(device_id)
                except AttributeError:
                    print("LikwidPowerSensor not installed")
                    sys.exit(1)
            elif platform == "nvml":
                try:
                    powersensor = ps.NVMLPowerSensor.create(device_id)
                except AttributeError:
                    print("NVMLPowerSensor not installed")
                    sys.exit(1)
            elif platform == "rapl":
                powersensor = ps.RaplPowerSensor.create()
            elif platform == "rocm":
                try: 
                    powersensor = ps.ROCMPowerSensor.create(device_id)
                except AttributeError:
                    print("ROCMPowerSensor not installed")
                    sys.exit(1)
            elif platform == "xilinx":
                powersensor = ps.XilinxPowerSensor.create()
            else:
                print("Invalid Option")
                sys.exit(1)

            start = powersensor.read()
            prev_results = func(*args, **kwargs)
            end = powersensor.read()

            results = []
            if prev_results is not None:
                results.append(prev_results[0])
            curr_results = {"platform" : platform,  \
                            "joules" : format(ps.joules(start, end),".3f"),  \
                            "seconds" : format(ps.seconds(start, end),".3f"), \
                            "watt" : format(ps.watt(start, end),".3f")}
            results.append(curr_results)

            return results
        return wrapper
    return decorator

def powersensor_dump(platform, filename, device_id = 0):
    def decorator(func):
        def wrapper(*args, **kwargs):
            if platform == "amdgpu":
                powersensor = ps.AMDGPUPowerSensor.create(device_id)
            elif platform == "arduino":
                try:
                    powersensor = ps.ArduinoPowerSensor.create(device_id)
                except AttributeError:
                    print("ArduinoPowerSensor not installed")
                    sys.exit(1)
            elif platform == "dummy":
                powersensor = ps.DummyPowerSensor.create()
            elif platform == "jetson":
                powersensor = ps.JetsonPowerSensor.create()
            elif platform == "likwid":
                try:
                    powersensor = ps.LikwidPowerSensor.create(device_id)
                except AttributeError:
                    print("LikwidPowerSensor not installed")
                    sys.exit(1)
            elif platform == "nvml":
                try:
                    powersensor = ps.NVMLPowerSensor.create(device_id)
                except AttributeError:
                    print("NVMLPowerSensor not installed")
                    sys.exit(1)
            elif platform == "rapl":
                powersensor = ps.RaplPowerSensor.create()
            elif platform == "rocm":
                try: 
                    powersensor = ps.ROCMPowerSensor.create(device_id)
                except AttributeError:
                    print("ROCMPowerSensor not installed")
                    sys.exit(1)
            elif platform == "xilinx":
                powersensor = ps.XilinxPowerSensor.create()
            else:
                print("Invalid Option")
                sys.exit(1)

            if(not filename):
                print("Please provide a filename to dump the results")
                sys.exit(1)
            start = powersensor.startDumpThread(filename)
            prev_results = func(*args, **kwargs)
            end = powersensor.stopDumpThread()

            return
        return wrapper
    return decorator

