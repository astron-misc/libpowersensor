#include <pybind11/pybind11.h>

#include <../common/PowerSensor.h>
#include <../amdgpu/AMDGPUPowerSensor.h>

#ifdef BUILD_ARDUINO
#include <../arduino2/Arduino2PowerSensor.h>
#endif

#include <../dummy/DummyPowerSensor.h>
#include <../jetson/JetsonPowerSensor.h>

#ifdef BUILD_LIKWID
#include <../likwid/LikwidPowerSensor.h>
#endif

#ifdef BUILD_NVML
#include <../nvml/NVMLPowerSensor.h>
#endif

#include <../rapl/RaplPowerSensor.h>

#ifdef BUILD_ROCM
#include <../rocm/ROCMPowerSensor.h>
#endif

#include <../xilinx/XilinxPowerSensor.h>

namespace py = pybind11;

double seconds(powersensor::State start, powersensor::State end){
    return end.timeAtRead - start.timeAtRead;
}

double joules(powersensor::State start, powersensor::State end){
    return end.joulesAtRead - start.joulesAtRead;
}

double watt(powersensor::State start, powersensor::State end){
    return joules(start, end) / seconds(start, end);
}

PYBIND11_MODULE(pypowersensor, m) {
    m.doc() = "libpowersensor python bindings"; 

    m.def("seconds", &seconds, "Get elapsed time");
    m.def("joules", &joules, "Get energy consumption");
    m.def("watt", &watt, "Get average power consumption");

    py::class_<powersensor::State>(m, "State");

    py::class_<powersensor::amdgpu::AMDGPUPowerSensor>(m, "AMDGPUPowerSensor")
        .def("create", &powersensor::amdgpu::AMDGPUPowerSensor::create)
	    .def("read", &powersensor::amdgpu::AMDGPUPowerSensor::read)
        .def("startDumpThread", &powersensor::amdgpu::AMDGPUPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::amdgpu::AMDGPUPowerSensor::stopDumpThread);
    
#ifdef BUILD_ARDUINO
    py::class_<powersensor::arduino2::Arduino2PowerSensor>(m, "ArduinoPowerSensor")
        .def("create", &powersensor::arduino2::Arduino2PowerSensor::create)
	    .def("read", &powersensor::arduino2::Arduino2PowerSensor::read)
        .def("startDumpThread", &powersensor::arduino2::Arduino2PowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::arduino2::Arduino2PowerSensor::stopDumpThread);
#endif

    py::class_<powersensor::DummyPowerSensor>(m, "DummyPowerSensor")
        .def("create", &powersensor::DummyPowerSensor::create)
	    .def("read", &powersensor::DummyPowerSensor::read)
        .def("startDumpThread", &powersensor::DummyPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::DummyPowerSensor::stopDumpThread);
         
    py::class_<powersensor::jetson::JetsonPowerSensor>(m, "JetsonPowerSensor")
        .def("create", &powersensor::jetson::JetsonPowerSensor::create)
	    .def("read", &powersensor::jetson::JetsonPowerSensor::read)
        .def("startDumpThread", &powersensor::jetson::JetsonPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::jetson::JetsonPowerSensor::stopDumpThread);

#ifdef BUILD_LIKWID
    py::class_<powersensor::likwid::LikwidPowerSensor>(m, "LikwidPowerSensor")
        .def("create", &powersensor::likwid::LikwidPowerSensor::create)
	    .def("read", &powersensor::likwid::LikwidPowerSensor::read)
        .def("startDumpThread", &powersensor::likwid::LikwidPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::likwid::LikwidPowerSensor::stopDumpThread);
#endif

#ifdef BUILD_NVML
    py::class_<powersensor::nvml::NVMLPowerSensor>(m, "NVMLPowerSensor")
        .def("create", &powersensor::nvml::NVMLPowerSensor::create)
	    .def("read", &powersensor::nvml::NVMLPowerSensor::read)
        .def("startDumpThread", &powersensor::nvml::NVMLPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::nvml::NVMLPowerSensor::stopDumpThread);
#endif

    py::class_<powersensor::rapl::RaplPowerSensor>(m, "RaplPowerSensor")
        .def("create", &powersensor::rapl::RaplPowerSensor::create)
	    .def("read", &powersensor::rapl::RaplPowerSensor::read)
        .def("startDumpThread", &powersensor::rapl::RaplPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::rapl::RaplPowerSensor::stopDumpThread);

#ifdef BUILD_ROCM
    py::class_<powersensor::rocm::ROCMPowerSensor>(m, "ROCMPowerSensor")
        .def("create", &powersensor::rocm::ROCMPowerSensor::create)
	    .def("read", &powersensor::rocm::ROCMPowerSensor::read)
        .def("startDumpThread", &powersensor::rocm::ROCMPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::rocm::ROCMPowerSensor::stopDumpThread);
#endif

    py::class_<powersensor::xilinx::XilinxPowerSensor>(m, "XilinxPowerSensor")
        .def("create", &powersensor::xilinx::XilinxPowerSensor::create)
	    .def("read", &powersensor::xilinx::XilinxPowerSensor::read)
        .def("startDumpThread", &powersensor::xilinx::XilinxPowerSensor::startDumpThread)
        .def("stopDumpThread", &powersensor::xilinx::XilinxPowerSensor::stopDumpThread);
}
