import time

from powersensor_decorator import powersensor

@powersensor('nvml')
def my_kernel():
   time.sleep(10)

if __name__ == "__main__":
   print(my_kernel())
