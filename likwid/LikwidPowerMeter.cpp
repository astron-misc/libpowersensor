#include "../common/PowerMeter.h"

#include "LikwidPowerSensor.h"

int main(int argc, char **argv)
{
    auto sensor = powersensor::likwid::LikwidPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}
