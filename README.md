# PowerSensor
This is a collection of `PowerSensor` implementations, based on the original `PowerSensor` code, available at https://gitlab.com/astron-misc/PowerSensor

# Installation
First clone the repository:
```
git clone --recursive git@gitlab.com:astron-misc/libpowersensor.git
```

The `--recursive` flag makes sure that the git submodules are also cloned. If you cloned the repository without this flag, you can initialize the submodules as follows:
```
git submodule init
git submodule update
```

To build the software, run the following commands:
1.  Set-up cmake in a build directory and cd into the directory, e.g. `/build/libpowersensor`
     * `$ cmake <source dir path> -DCMAKE_INSTALL_PREFIX=<install dir path>`
2.  Optionally further configure cmake through interactive build settings or with command line variables
     * use `ccmake` and/or add `-DBUILD_<SENSOR>_POWERSENSOR=<0 or 1>` to the `cmake` commandline to select which PowerSensors are built.
3.  make and install
     * `$ make install`

# Usage
Include the header file into your program, e.g.:
```
#include "powersensor.h"
```

All PowerSensor instances are contained in a namespace:
```
using namespace powersensor;
```

Depending on which PowerSensor implementations you have selected during the build, you can now initialize
any PowerSensor instance:
```
PowerSensor sensor = arduino2::ArduinoPowerSensor::create("/dev/ttyACM0")
```

Next, you can start measuring power using the common api as specified in `PowerSensor.h`:
```
PowerSensor::State start = sensor.read();
...
PowerSensor::State stop = sensor.read();
std::cout << "The computation took " << PowerSensor::Joules(start, stop) << 'J' << std::endl;
```

# PowerMeter
PowerMeter executables might be used direclty to read the values of a PowerSensor at a regular interval. The PowerMeter executables are available in the install directory `/bin`.
