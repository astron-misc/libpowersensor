#include "Arduino3PowerSensor.h"

/*
    Include original PowerSensor header file
    The _ prefixed aliases prevents class and
    namespace conflicts.
*/
#include <PowerSensor3/host/include/PowerSensor.hpp>
using _PowerSensor = PowerSensor3::PowerSensor;
using _State = PowerSensor3::State;


namespace powersensor {
namespace arduino3 {

class Arduino3PowerSensor_ : public Arduino3PowerSensor {
    public:
        Arduino3PowerSensor_(const char *device);
        ~Arduino3PowerSensor_();

        State measure();

    private:
        double (&_seconds)(const _State&, const _State&) = PowerSensor3::seconds;
        double (&_Joules)(const _State&, const _State&, int) = PowerSensor3::Joules;

        virtual const char* getDumpFileName() {
            return "/tmp/arduino3powersensor.out";
        }

        virtual int getDumpInterval() {
            return 1; // milliseconds
        }


        _PowerSensor* _powersensor;
        _State _firstState;
};

Arduino3PowerSensor* Arduino3PowerSensor::create(
    const char *device)
{
    return new Arduino3PowerSensor_(device);
}

Arduino3PowerSensor_::Arduino3PowerSensor_(
    const char *device)
{
    _powersensor = new _PowerSensor(device);
    _firstState = _powersensor->read();
}

Arduino3PowerSensor_::~Arduino3PowerSensor_() {
    delete _powersensor;
}

powersensor::State Arduino3PowerSensor_::measure() {
    _State _state = _powersensor->read();
    powersensor::State state;
    state.timeAtRead   = _seconds(_firstState, _state);
    state.joulesAtRead = _Joules(_firstState, _state, -1);
    return state;
}

} // end namespace arduino3
} // end namespace powersensor
