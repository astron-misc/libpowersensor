#ifndef ARDUINO_POWER_SENSOR_H_
#define ARDUINO_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    namespace arduino3 {
        class Arduino3PowerSensor : public PowerSensor {
            public:
                static Arduino3PowerSensor* create(
                    const char *device = default_device().c_str());

                static std::string default_device() {
                    return "/dev/ttyACM0";
                }
        };
    } // end namespace arduino3
} // end namespace powersensor

#endif
