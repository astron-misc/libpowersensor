#include "../common/PowerMeter.h"

#include "Arduino3PowerSensor.h"

int main(int argc, char *argv[])
{
    auto sensor = powersensor::arduino3::Arduino3PowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}
