#ifndef ARDUINO_POWER_SENSOR_H_
#define ARDUINO_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    namespace arduino2 {
        class Arduino2PowerSensor : public PowerSensor {
            public:
                static Arduino2PowerSensor* create(
                    const char *device = default_device().c_str());

                static std::string default_device() {
                    return "/dev/ttyACM0";
                }
        };
    } // end namespace arduino2
} // end namespace powersensor

#endif
