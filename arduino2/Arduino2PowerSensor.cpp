#include "Arduino2PowerSensor.h"

/*
    Include original PowerSensor header file
    The _ prefixed aliases prevents class and
    namespace conflicts.
*/
#include <PowerSensor2/PowerSensor.h>
using _PowerSensor = PowerSensor::PowerSensor;
using _State = PowerSensor::State;


namespace powersensor {
namespace arduino2 {

class Arduino2PowerSensor_ : public Arduino2PowerSensor {
    public:
        Arduino2PowerSensor_(const char *device);
        ~Arduino2PowerSensor_();

        State measure();

    private:
        double (&_seconds)(const _State&, const _State&) = ::PowerSensor::seconds;
        double (&_Joules)(const _State&, const _State&, int) = ::PowerSensor::Joules;

        virtual const char* getDumpFileName() {
            return "/tmp/arduino2powersensor.out";
        }

        virtual int getDumpInterval() {
            return 1; // milliseconds
        }


        _PowerSensor* _powersensor;
        _State _firstState;
};

Arduino2PowerSensor* Arduino2PowerSensor::create(
    const char *device)
{
    return new Arduino2PowerSensor_(device);
}

Arduino2PowerSensor_::Arduino2PowerSensor_(
    const char *device)
{
    _powersensor = new _PowerSensor(device);
    _firstState = _powersensor->read();
}

Arduino2PowerSensor_::~Arduino2PowerSensor_() {
    delete _powersensor;
}

powersensor::State Arduino2PowerSensor_::measure() {
    _State _state = _powersensor->read();
    powersensor::State state;
    state.timeAtRead   = _seconds(_firstState, _state);
    state.joulesAtRead = _Joules(_firstState, _state, -1);
    return state;
}

} // end namespace arduino2
} // end namespace powersensor
