#include "../common/PowerMeter.h"

#include "Arduino2PowerSensor.h"

int main(int argc, char *argv[])
{
    auto sensor = powersensor::arduino2::Arduino2PowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}
