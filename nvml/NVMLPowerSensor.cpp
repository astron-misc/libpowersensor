#include "NVMLPowerSensor.h"

#include <iostream>
#include <stdexcept>
#include <sstream>

#include <unistd.h>

#if defined(HAVE_NVML)
#include "nvml.h"

#define checkNVMLCall(val)  __checkNVMLCall((val), #val, __FILE__, __LINE__)

inline void __checkNVMLCall(
    nvmlReturn_t result,
    const char *const func,
    const char *const file,
    int const line)
{
    if (result != NVML_SUCCESS) {
        std::stringstream error;
        error << "NVML Error at " << file;
        error << ":" << line;
        error << " in function " << func;
        error << ": " << nvmlErrorString(result);
        error << std::endl;
        throw std::runtime_error(error.str());
    }
}
#endif

namespace powersensor {
namespace nvml {

class NVMLPowerSensor_ : public NVMLPowerSensor {
    public:
        NVMLPowerSensor_(int device_number);
        ~NVMLPowerSensor_();

    private:
        class NVMLState {
            public:
                operator State();
                double timeAtRead;
                unsigned int instantaneousPower   = 0;
                unsigned int consumedEnergyDevice = 0;
        };

        virtual State measure();

        virtual const char* getDumpFileName() {
            return "/tmp/nvmlpowersensor.out";
        }

        virtual int getDumpInterval() {
            return 10; // milliseconds
        }

        NVMLState previousState;
        NVMLState read_nvml();

#if defined(HAVE_NVML)
        nvmlDevice_t device;
#endif
};

NVMLPowerSensor_::NVMLState::operator State()
{
    State state;
    state.timeAtRead = timeAtRead;
    state.joulesAtRead = consumedEnergyDevice * 1e-3;
    return state;
}

NVMLPowerSensor* NVMLPowerSensor::create(
    int device_number)
{
    return new NVMLPowerSensor_(device_number);
}

NVMLPowerSensor_::NVMLPowerSensor_(
    int device_number)
{
    char *cstr_powersensor_device = getenv("POWERSENSOR_DEVICE");
    unsigned device_number_ = cstr_powersensor_device ? atoi(cstr_powersensor_device) : device_number;

#if defined(HAVE_NVML)
    checkNVMLCall(nvmlInit());
    checkNVMLCall(nvmlDeviceGetHandleByIndex(device_number_, &device));
#endif

    previousState = read_nvml();
    previousState.consumedEnergyDevice = 0;
}

NVMLPowerSensor_::~NVMLPowerSensor_() {
#if defined(HAVE_NVML)
    stopDumpThread();
    checkNVMLCall(nvmlShutdown());
#endif
}

NVMLPowerSensor_::NVMLState NVMLPowerSensor_::read_nvml() {
    NVMLState state;
    state.timeAtRead = get_wtime();

#if defined(HAVE_NVML)
    checkNVMLCall(nvmlDeviceGetPowerUsage(device, &state.instantaneousPower));
    state.consumedEnergyDevice = previousState.consumedEnergyDevice;
    float averagePower = (state.instantaneousPower + previousState.instantaneousPower) / 2;
    float timeElapsed = (state.timeAtRead - previousState.timeAtRead);
    state.consumedEnergyDevice += averagePower * timeElapsed;
#else
    state.consumedEnergyDevice = 0;
#endif
    
    previousState = state;

    return state;
}

State NVMLPowerSensor_::measure() {
    return read_nvml();
}

} // end namespace nvml
} // end namespace powersensor
